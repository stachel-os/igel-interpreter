#ifndef _LIBIGI_COMPILER_H_
#define _LIBIGI_COMPILER_H_

#include <stdint.h>
#include <stdio.h>
#include <libigi/program.h>

typedef FILE *(*IgelIncludeFunc)(const char *name, void *userdata);

typedef struct _IgelBuiltinFunction {
	const char *argline, *name;
	char return_type;
} IgelBuiltinFunction;

typedef struct _IgelCompiler IgelCompiler;

IgelProgram *igc_compile(FILE *stream,
			 const char *name,
			 IgelIncludeFunc include_file,
			 void *userdata,
			 const IgelBuiltinFunction **funcs,
			 size_t n_funcs);
uint16_t igc_resolve_function(IgelProgram *prg,
			      const char *name);
void igc_free_program(IgelProgram *prg);
void igc_dispose(IgelCompiler *igc);

#endif
