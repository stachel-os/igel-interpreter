#ifndef _LIBIGI_VM_H_
#define _LIBIGI_VM_H_

#include <stdint.h>
#include <libigi/program.h>

typedef struct _IgelVM IgelVM;

typedef void (*IgelVMBuiltinFunction)(IgelVM *vm, size_t fp, void *userdata);

int igvm_run_program(IgelProgram *prg,
		     uint16_t marker,
		     const IgelVMBuiltinFunction *funcs,
		     size_t n_funcs,
		     void *userdata);
double igvm_get_double(IgelVM *vm, size_t offset);
void igvm_put_double(IgelVM *vm, size_t offset, double val);
float igvm_get_float(IgelVM *vm, size_t offset);
void igvm_put_float(IgelVM *vm, size_t offset, float val);
uint64_t igvm_get64(IgelVM *vm, size_t offset);
void igvm_put64(IgelVM *vm, size_t offset, uint64_t val);
uint32_t igvm_get32(IgelVM *vm, size_t offset);
void igvm_put32(IgelVM *vm, size_t offset, uint32_t val);
uint16_t igvm_get16(IgelVM *vm, size_t offset);
void igvm_put16(IgelVM *vm, size_t offset, uint16_t val);
uint8_t igvm_get8(IgelVM *vm, size_t offset);
void igvm_put8(IgelVM *vm, size_t offset, uint8_t val);
const char *igvm_get_string(IgelVM *vm, size_t offset);

#endif
