/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "igerror.h"
#define IGLEX_PRIVATE
#include "iglex.h"

struct _IgLex {
	IgLex *inc;
	IgLexToken *inc_name_token;
	IgelIncludeFunc include_file;
	void *userdata;
	FILE *stream;
	IgelErrorStatus *err;
	const char *name;
	int line, eof, very_first_token, close_stream;
};

typedef struct _IgLexToken {
	IGLEX_TOKEN_STRUCT;
	IgLex *lex;
	uint8_t refcount;
} IgLexToken;

IgLex *iglex_create(IgelErrorStatus *err,
		    FILE *stream,
		    int close_stream,
		    const char *name,
		    IgelIncludeFunc include_file,
		    void *userdata)
{
	IgLex *iglex = malloc(sizeof(IgLex));
	iglex->inc = NULL;
	iglex->include_file = include_file;
	iglex->userdata = userdata;
	iglex->err = err;
	iglex->stream = stream;
	iglex->close_stream = close_stream;
	iglex->name = name;
	iglex->line = 1;
	iglex->eof = 0;
	iglex->very_first_token = 1;
	return iglex;
}

static int handle_single(IgLexToken *token, int c, IgLexTokenType type)
{
	token->type = type;
	token->repr = malloc(2);
	token->repr[0] = c;
	token->repr[1] = '\0';
	return 1;
}

static int handle_dual(IgLexToken *token, int c, char match, IgLexTokenType type)
{
	char c2 = fgetc(token->lex->stream);
	if (c2 == match) {
		token->type = type;
		token->repr = malloc(3);
		token->repr[0] = c;
		token->repr[1] = c2;
		token->repr[2] = '\0';
		return 1;
	}
	ungetc(c2, token->lex->stream);
	return 0;
}

static int handle_identifier(IgLexToken *token, int c)
{
	size_t bufsize = 128;
	int i;
	token->type = IDENTIFIER;
	token->repr = malloc(bufsize);
	token->repr[0] = c;
	for (i = 1;; i++) {
		if ((size_t)i >= bufsize) {
			bufsize *= 2;
			token->repr = realloc(token->repr, bufsize);
		}
		c = fgetc(token->lex->stream);
		if (c == ']' || c == '[' || c == '}' || c == '{' ||
		    c == '"' || c == '%' || c == ' ' || c == '\t' ||
		    c == '\n' || c == EOF)
		{
			break;
		}
		token->repr[i] = c;
	}
	if (c != EOF)
		ungetc(c, token->lex->stream);
	token->repr[i] = '\0';

	if (0 == strcmp(token->repr, ".j0"))
		token->type = V_JUMP_ZERO;
	else if (0 == strcmp(token->repr, ".j1"))
		token->type = V_JUMP_NONZERO;
	else if (0 == strcmp(token->repr, ".j"))
		token->type = V_JUMP;
	else if (0 == strcmp(token->repr, ".start"))
		token->type = V_START;
	else if (0 == strcmp(token->repr, ".end"))
		token->type = V_END;
	return 1;
}

static int handle_string(IgLexToken *token, int c)
{
	size_t bufsize = 128;
	int i;
	token->type = STRING;
	token->repr = malloc(bufsize);
	for (i = 0;; i++) {
		if ((size_t)i >= bufsize) {
			bufsize *= 2;
			token->repr = realloc(token->repr, bufsize);
		}
		c = fgetc(token->lex->stream);
		if (c == EOF) {
			igerror_report_token(token->lex->err, "Lexical Error",
					     token,
					     "Missing terminating \" character");
			break;
		}
		if (c == '"') {
			c = fgetc(token->lex->stream);
			if (c != '"') break;
			// "" in string escapes "
		}
		token->repr[i] = c;
	}
	if (c != EOF)
		ungetc(c, token->lex->stream);
	token->repr[i] = '\0';
	return 1;
}

static int handle_percent_sign(IgLexToken *token, int c)
{
	c = fgetc(token->lex->stream);
	if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')) {
		token->type = ARG_REF;
	} else {
		ungetc(c, token->lex->stream);
		return 0;
	}
	token->repr = malloc(2);
	token->repr[0] = c;
	token->repr[1] = '\0';
	return 1;
}

static int handle_comment(IgLexToken *token, int c, int allow_inc)
{
	c = fgetc(token->lex->stream);
	if (c == '<' && allow_inc) {
		FILE *file;
		IgLexToken *name_token = iglex_next_token(token->lex);
		if (name_token->type != STRING) {
			igerror_report_token(token->lex->err,
					     "Fatal Syntax Error",
					     name_token,
					     "Expected name of file to include");
			token->type = EOF_TOKEN;
			iglex_token_unref(name_token);
			return 1;
		}
		if (token->lex->include_file) {
			file = token->lex->include_file(
				name_token->repr,
				token->lex->userdata);
		} else {
			file = fopen(name_token->repr, "r");
		}
		token->lex->inc_name_token = name_token;
		if (!file) {
			igerror_report_token(token->lex->err,
					     "Fatal Error",
					     name_token,
					     "Unable to include '%s'",
					     name_token->repr);
			token->type = EOF_TOKEN;
			token->lex->eof = 1;
			iglex_token_unref(name_token);
			return 1;
		}
		token->lex->inc = iglex_create(token->lex->err,
					       file,
					       1,
					       name_token->repr,
					       token->lex->include_file,
					       token->lex->userdata);
	}
	while (c != '\n' && c != EOF) {
		c = fgetc(token->lex->stream);
	}
	token->line = token->lex->line += 1;
	return 0;
}

static int handle_unexpected(IgLexToken *token, int c, int read_ahead)
{
	while (read_ahead) {
		read_ahead--;
		c = fgetc(token->lex->stream);
	}

	igerror_report_token(token->lex->err, "Lexical Error",
			     token, "Unexpected character: '%c'",
			     c);
	return 0;
}

IgLexToken *iglex_next_token(IgLex *iglex)
{
	IgLexToken *token;
	int c;

	if (iglex->inc) {
		token = iglex_next_token(iglex->inc);
		if (token->type == EOF_TOKEN){
			iglex_token_unref(token);
			iglex_token_unref(iglex->inc_name_token);
			iglex_dispose(iglex->inc);
			iglex->inc = NULL;
			iglex->inc_name_token = NULL;
		} else {
			return token;
		}
	}

	if (iglex->eof) {
		return 0;
	}

	token = malloc(sizeof(IgLexToken));
	token->refcount = 1;
	token->lex = iglex;
	token->line = iglex->line;
	token->file = iglex->name;
	token->repr = NULL;

	while (1) {
		c = fgetc(iglex->stream);

		switch (c) {
		case EOF:	token->type = EOF_TOKEN;
				iglex->eof = 1;
				break;

#define TOKEN(_format, ...) if (handle_##_format(token, c, ##__VA_ARGS__)) break
#define STOP assert(0 && "unhandled condition!")

		case '[':	TOKEN(single, OPENING_BRACKET); STOP;
		case ']':	TOKEN(single, CLOSING_BRACKET); STOP;
		case '{':	TOKEN(single, OPENING_CURLY); STOP;
		case '}':	TOKEN(single, CLOSING_CURLY); STOP;
		case '#':	if (iglex->very_first_token) {
					/* allow #!/path/to/interpreter */
					TOKEN(comment, 0);
					iglex->very_first_token = 0;
					continue;
				} else {
					TOKEN(single, VALUE_MARK);
				}
				STOP;

		case ';':	TOKEN(comment, 1);
				if (iglex->inc) {
					iglex_token_unref(token);
					return iglex_next_token(iglex);
				}
				continue;

		case '.':	TOKEN(dual, '[', OPENING_DOT_BRACKET);
				TOKEN(identifier);
				STOP;
		case '_':	TOKEN(dual, '[', OPENING_UND_BRACKET);
				TOKEN(identifier);
				STOP;

		case '-':	TOKEN(dual, '>', STORE_VALUE_TO);
				TOKEN(identifier);
				STOP;

		case '"':	TOKEN(string);
				STOP;

		case '%':	TOKEN(percent_sign);
				TOKEN(unexpected, 1);
				continue;

		case '\\':	TOKEN(dual, 'Z', TYPE_S32);
				TOKEN(dual, 'N', TYPE_U32);
				TOKEN(dual, 'z', TYPE_S16);
				TOKEN(dual, 'n', TYPE_U16);
				TOKEN(dual, '7', TYPE_S8);
				TOKEN(dual, '8', TYPE_U8);
				TOKEN(dual, 'R', TYPE_F64);
				TOKEN(dual, 'r', TYPE_F32);
				TOKEN(unexpected, 1);
				break;

		case '!':
		case '$':
		case '&':
		case '\'':
		case '(':
		case ')':
		case '*':
		case '+':
		case ',':
		case '/':
		case '0'...'9':
		case ':':
		case '<':
		case '=':
		case '>':
		case 'a'...'z':
		case 'A'...'Z':	TOKEN(identifier);
				STOP;

		case '\n':	token->line = iglex->line += 1;
		case ' ':
		case '\t':
		case '\r':	continue;

		default:	TOKEN(unexpected, 0);
				continue;
#undef STOP
#undef TOKEN
		}
		iglex->very_first_token = 0;
		break;
	}

	return token;
}

IgLexToken *iglex_token_ref(IgLexToken *token)
{
	token->refcount++;
	return token;
}

void iglex_token_unref(IgLexToken *token)
{
	if (!token) return;
	assert(token->refcount > 0);
	token->refcount--;
	if (token->refcount < 1) {
		free(token->repr);
		free(token);
	}
}

void iglex_dispose(IgLex *iglex)
{
	if (iglex->close_stream)
		fclose(iglex->stream);
	free(iglex);
}
