/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGTABLE_H_
#define _LIBIGI_PRIVATE_IGTABLE_H_

#include "igname.h"

typedef struct _IgTable IgTable;
typedef void (IgTableValueDestroyFunc)(void *);

IgTable *igtable_new(IgTableValueDestroyFunc *vdf);
void igtable_put(IgTable *table, IgName *key, void *value);
void *igtable_get(IgTable *table, IgName *key);
void igtable_dispose(IgTable *table);

#endif
