/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_PROGRAM_H_
#define _LIBIGI_PRIVATE_PROGRAM_H_

#include <assert.h>
#include <stdint.h>
#include "igtable.h"

struct _IgelProgram {
	IgTable *private, *public;
	uint32_t *markers;
	uint16_t n_markers;
	uint32_t *bytecode;
	uint32_t last_addr;
	uint8_t *data;
	uint32_t last_daddr;
};

#define VALID_ADDR (1 << 31) /* for data addresses */

static inline uint16_t build_addr(uint8_t size, uint16_t addr)
{
	assert(size == 1 || size == 2 || size == 4 || size == 8);
	while (size >>= 1) {
		addr = addr >> 1 | (1 << 14);
	}
	return addr << 1;
}

static inline void unpack_addr(uint8_t *size,
				   uint16_t *addr,
				   uint16_t *index)
{
	uint16_t mask = 0x3fff;
	*size = 1;
	*addr >>= 1;
	if (index)
		*index = *addr;
	while (*addr & (1 << 14)) {
		*addr = (*addr << 1) & 0x7fff;
		mask >>= 1;
		*size <<= 1;
		if (*size == 8)
			break;
	}
	if (index)
		*index &= mask;
}

#define I_VAL_PUT 0
#define I_VAL_TEST 1
#define I_VAL_EXT1 2
#define I_VAL_EXT1_RET 0

/* Assembly macros */

#define M(_d) (0xf000 | ((_d) << 1))
#define N(_d) (0xf000 | ((_d) << 1) | 1)
#define MN(_d) (0xf000 | (_d))
#define A(_d) (0xe000 | ((_d) << 1))
#define B(_d) (0xe000 | ((_d) << 1) | 1)
#define AB(_d) (0xe000 | (_d))
#define AA(_s, _p) (build_addr(_s, _p))
#define BB(_s, _p) (build_addr(_s, _p) | 1)

#define I(_l, _r) ((_l) << 16 | (_r))

#define I_CALL(_m, _p) I(_m, M((_p) >> 3))
#define I_JMP(_m, _t) I(_m, N(_t))

#define I_COPY(_ds, _dp, _ss, _sp) I(AA(_ds, _dp), AA(_ss, _sp))
#define I_PAD(_ds, _dp, _ss, _sp) I(BB(_ds, _dp), AA(_ss, _sp))

#define I_EXT(_n, _p) I(MN(_n), A((_p) >> 3))

#define I_RET I(MN(I_VAL_EXT1), B(I_VAL_EXT1_RET))
#define I_TEST(_s, _p) I(MN(I_VAL_TEST), BB(_s, _p))
#define I_PUT(_s, _p) I(MN(I_VAL_PUT), BB(_s, _p))

/* Disassembly macros */

#define LEFT(_i) ((_i) >> 16)
#define RIGHT(_i) ((_i) & 0xffff)

#define IS_MN(_i) (((_i) & 0xf000) == 0xf000)
#define IS_AB(_i) (((_i) & 0xf000) == 0xe000)
#define IS_AA(_i) (!IS_MN(_i) && !((_i) & 1))
#define IS_BB(_i) (!IS_MN(_i) && ((_i) & 1))
#define IS_A(_i) (IS_AB(_i) && !((_i) & 1))
#define IS_B(_i) (IS_AB(_i) && ((_i) & 1))
#define IS_M(_i) (IS_MN(_i) && !((_i) & 1))
#define IS_N(_i) (IS_MN(_i) && ((_i) & 1))

#define GET_HIGHER(_i) ((_i) & 0xfff)
#define GET_LOWER(_i) (GET_HIGHER(_i) >> 1)

#endif
