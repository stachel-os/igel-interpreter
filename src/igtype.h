/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGTYPE_H_
#define _LIBIGI_PRIVATE_IGTYPE_H_

typedef enum _IgType {
	IGTYPE_NONE,
	IGTYPE_S32,
	IGTYPE_U32,
	IGTYPE_S16,
	IGTYPE_U16,
	IGTYPE_S8,
	IGTYPE_U8,
	IGTYPE_F64,
	IGTYPE_F32,
	IGTYPE_PTR,
	IGTYPE_MAX_SIZE,
} IgType;

static inline int is_sinttype(IgType type)
{
	return type == IGTYPE_S32 ||
	       type == IGTYPE_S16 ||
	       type == IGTYPE_S8;
}

static inline int is_uinttype(IgType type)
{
	return type == IGTYPE_U32 ||
	       type == IGTYPE_U16 ||
	       type == IGTYPE_U8;
}

static inline int is_inttype(IgType type)
{
	return is_sinttype(type) || is_uinttype(type);
}

static inline int is_floattype(IgType type)
{
	return type == IGTYPE_F64 ||
	       type == IGTYPE_F32;
}

const char *igtype_to_string(IgType type);
unsigned int igtype_get_size(IgType type);

static inline IgType igtype_from_char(char c)
{
	return c == 'Z' ? IGTYPE_S32 :
	       c == 'N' ? IGTYPE_U32 :
	       c == 'z' ? IGTYPE_S16 :
	       c == 'n' ? IGTYPE_U16 :
	       c == '7' ? IGTYPE_S8 :
	       c == '8' ? IGTYPE_U8 :
	       c == 'R' ? IGTYPE_F64 :
	       c == 'r' ? IGTYPE_F32 :
	       c == '^' ? IGTYPE_PTR :
	       IGTYPE_NONE;
}

#endif
