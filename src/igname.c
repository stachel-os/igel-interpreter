/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#define IGNAME_PRIVATE
#include "igname.h"

struct _IgName {
	IGNAME_STRUCT;
	uint8_t refcount;
};

IgName *igname_new(void)
{
	IgName *name = malloc(sizeof(IgName));
	name->size = 32;
	name->tail = 0;
	name->data = malloc(name->size);
	name->data[0] = '\0';
	name->refcount = 1;
	name->hash = 0;
	return name;
}

static uint32_t hash_string(const char *str)
{
	uint32_t hash = 0xffffffff;
	unsigned int i;
	for (i = 0; str[i]; i++) {
		hash ^= str[i];
		hash *= 0x1000193;
	}
	for (i = 0; str[i]; i++) {
		hash ^= str[i];
		hash *= 0x1000193;
	}
	return hash;
}

int igname_equal(IgName *a, IgName *b)
{
	char *str1 = a->data, *str2 = b->data;
	unsigned int i;
	if (a->hash != b->hash)
		return 0;
	if (str1 == str2)
		return 1;
	if (a->tail != b->tail)
		return 0;
	for (i = 0; i < a->tail; i++) {
		while (str1[i] && str1[i] == str2[i])
			i++;
		if (str1[i] != str2[i])
			return 0;
	}
	return 1;
}

void igname_put_string(IgName *name, const char *str)
{
	name->hash ^= hash_string(str);
	while (*str) {
		if (name->tail >= name->size - 1) {
			if (name->size == 0xffff) {
				fprintf(stderr, "igname: ERROR: cannot grow name\n");
				name->data[0xfffe] = '\0';
				return;
			}
			name->size *= 2;
			if (name->size == 0) {
				name->size -= 1;
			}
			name->data = realloc(name->data, name->size);
		}
		name->data[name->tail++] = *(str++);
	}
	name->data[name->tail++] = '\0';
}

void igname_print(IgName *name)
{
	char *str = name->data;
	unsigned int i;
	printf("[");
	for (i = 0; i < name->tail; i++) {
		printf("'%s', ", str+i);
		i += strlen(str+i);
	}
	printf("]\n");
}

static void rehash(IgName *name)
{
	char *str = name->data;
	unsigned int i;
	name->hash = 0;
	for (i = 0; i < name->tail; i++) {
		name->hash ^= hash_string(str+i);
		i += strlen(str+i);
	}
}

const char *igname_join(IgName *name, char ch)
{
	char *str = name->data;
	unsigned int i;
	for (i = 0; i < name->tail; i++) {
		i += strlen(str+i);
		if (i + 1 != name->tail)
			str[i] = ch;
	}
	rehash(name);
	return str;
}

void igname_split(IgName *name, char ch)
{
	char *str = name->data;
	unsigned int i;
	for (i = 0; i < name->tail; i++) {
		if (str[i] == ch)
			str[i] = '\0';
	}
	rehash(name);
}

IgName *igname_ref(IgName *name)
{
	name->refcount++;
	return name;
}

void igname_unref(IgName *name)
{
	if (!name) return;
	assert(name->refcount > 0);
	name->refcount--;
	if (name->refcount < 1) {
		free(name->data);
		free(name);
	}
}
