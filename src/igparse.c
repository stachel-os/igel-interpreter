/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "igerror.h"
#include "igparse.h"
#include "igtable.h"

//#define MACRO_DEBUG

typedef struct _IgTokenList {
	struct _IgTokenList *next;
	IgLexToken *token;
} IgTokenList;

struct _IgParse {
	IgelErrorStatus *err;
	const IgParseHandlers *handlers;
	void *userdata;
	IgLex *lex;
	IgTable *macros;
	IgLexToken *next;
	IgTokenList *inserted, **ins_link, *ins_tail;
};

static void destroy_macro(void *data);

IgParse *igparse_create(IgelErrorStatus *err,
			const IgParseHandlers *handlers,
			void *userdata)
{
	IgParse *igparse = calloc(1, sizeof(IgParse));
	igparse->err = err;
	igparse->handlers = handlers;
	igparse->userdata = userdata;
	igparse->macros = igtable_new(destroy_macro);
	igparse->ins_link = &(igparse->inserted);
	return igparse;
}

#define parse_error(_p, _fmt, ...) \
	igerror_report_token((_p)->err, "Syntax Error", \
			     (_p)->next, _fmt, ##__VA_ARGS__)

static void insert(IgParse *igparse, IgLexToken *token)
{
	*igparse->ins_link = malloc(sizeof(IgTokenList));
	(*igparse->ins_link)->next = igparse->ins_tail;
	(*igparse->ins_link)->token = iglex_token_ref(token);
	igparse->ins_link = &(*igparse->ins_link)->next;
#ifdef MACRO_DEBUG
	fprintf(stderr, "***** %s\n", token->repr);
#endif
}

static void next(IgParse *igparse)
{
	if (!igparse->next || igparse->next->type != EOF_TOKEN) {
		iglex_token_unref(igparse->next);
		if (igparse->inserted) {
			IgTokenList *item = igparse->inserted;
			igparse->next = item->token;
			igparse->inserted = item->next;
			igparse->ins_link = &(igparse->inserted);
			igparse->ins_tail = igparse->inserted;
			free(item);
		} else {
			assert(igparse->lex);
			igparse->next = iglex_next_token(igparse->lex);
		}
	}
#ifdef MACRO_DEBUG
	fprintf(stderr, "TOKEN %s\n", igparse->next->repr);
#endif
}

static void finish_insert(IgParse *igparse)
{
	/* Put next token after inserted sequence */
	insert(igparse, igparse->next);
	/* Skip forward to inserted sequence */
	next(igparse);
}

static int consume(IgParse *igparse, IgLexTokenType type)
{
	if (igparse->next->type == type) {
		next(igparse);
		return 1;
	}

	return 0;
}

static IgTokenList *parse_token_sequence(IgParse *igparse, int curly)
{
	IgTokenList *head, **tail = &head;
	unsigned int nesting = 1;

	while (igparse->next->type != EOF_TOKEN) {
		if (curly ? igparse->next->type == OPENING_CURLY
			  : (igparse->next->type == OPENING_BRACKET ||
			     igparse->next->type == OPENING_UND_BRACKET ||
			     igparse->next->type == OPENING_DOT_BRACKET))
			nesting++;
		else if (igparse->next->type ==
			 (curly ? CLOSING_CURLY : CLOSING_BRACKET) &&
			 --nesting == 0)
			break;
		*tail = malloc(sizeof(IgTokenList));
		(*tail)->token = iglex_token_ref(igparse->next);
		tail = &(*tail)->next;
		next(igparse);
	}

	*tail = NULL;

	return head;
}

static void destroy_token_sequence(IgTokenList *tokens)
{
	IgTokenList *tmp;
	while (tokens) {
		tmp = tokens;
		tokens = tmp->next;
		iglex_token_unref(tmp->token);
		free(tmp);
	}
}

typedef struct _IgMacro {
	unsigned int argc;
	IgTokenList *tokens;
} IgMacro;

static void destroy_macro(void *data)
{
	IgMacro *macro = data;
	destroy_token_sequence(macro->tokens);
	free(macro);
}

static int parse_value(IgParse *igparse);

static void parse_store(IgParse *igparse)
{
	if (igparse->next->type == IDENTIFIER) {
		IgName *name = igname_new();
		igname_put_string(name, igparse->next->repr);
		igparse->handlers->store(igparse->userdata, name);
		igname_unref(name);
		next(igparse);
	} else {
		parse_error(igparse,
			    "Expected variable name after '->', found '%s'",
			    igparse->next->repr);
	}
}

static int parse_call(IgParse *igparse, int public)
{
	IgName *name = igname_new();

	igparse->handlers->function_call_prepare(igparse->userdata);

	while (igparse->next->type != EOF_TOKEN) {
		if (consume(igparse, OPENING_BRACKET)) {
			igname_put_string(name, "[]");
			if (parse_value(igparse) < 0)
				goto err;
		} else if (consume(igparse, CLOSING_BRACKET)) {
			break;
		} else if (igparse->next->type == IDENTIFIER) {
			igname_put_string(name, igparse->next->repr);
			next(igparse);
		} else {
			parse_error(igparse, "Unexpected '%s' in name",
				    igparse->next->repr);
			next(igparse);
		}
	}

	igparse->handlers->function_call(igparse->userdata, name, public);

	if (consume(igparse, STORE_VALUE_TO))
		parse_store(igparse);

	igname_unref(name);
	return 0;
err:
	igname_unref(name);
	return -1;
}

static void parse_macro_call(IgParse *igparse)
{
	IgName *name = igname_new();
	IgTokenList *args[10];
	IgMacro *macro;
	unsigned int argc = 0;

	while (igparse->next->type != EOF_TOKEN) {
		if (consume(igparse, OPENING_BRACKET)) {
			IgTokenList *tokens;
			tokens = parse_token_sequence(igparse, 0);
			if (argc >= 10) {
				parse_error(igparse,
					    "Macro cannot have >10 arguments");
			} else {
				igname_put_string(name, "[]");
				args[argc++] = tokens;
			}
			if (!consume(igparse, CLOSING_BRACKET))
				parse_error(igparse, "Missing ] after argument");
		} else if (consume(igparse, CLOSING_BRACKET)) {
			break;
		} else if (igparse->next->type == IDENTIFIER) {
			igname_put_string(name, igparse->next->repr);
			next(igparse);
		} else {
			parse_error(igparse, "Unexpected '%s' in macro name",
				    igparse->next->repr);
			next(igparse);
		}
	}

	macro = igtable_get(igparse->macros, name);

	if (macro) {
		IgTokenList *tmp = macro->tokens, *back = NULL;
		while (tmp) {
			if (tmp->token->type == ARG_REF) {
				unsigned int argi = tmp->token->repr[0] - '0';
				if (back) {
					parse_error(igparse,
						    "Macro parameter nesting not allowed");
					tmp = tmp->next;
					continue;
				}
				if (argi >= argc) {
					parse_error(igparse,
						    "Macro parameter out of bounds: %d",
						    argi);
					tmp = tmp->next;
					continue;
				}
				if (args[argi]) {
					back = tmp->next;
					tmp = args[argi];
					continue;
				}
			} else {
				insert(igparse, tmp->token);
			}
			tmp = tmp->next;
			if (!tmp) {
				tmp = back;
				back = NULL;
			}
		}
		finish_insert(igparse);
	} else {
		parse_error(igparse, "Undeclared macro: %s",
			    igname_join(name, ' '));
	}

	while (argc) {
		destroy_token_sequence(args[argc-1]);
		argc--;
	}

	igname_unref(name);
}

static int check_srange(IgType type, long integer)
{
	unsigned int size = igtype_get_size(type);
	long mask;
	if (size == sizeof(unsigned long))
		return 1;
	mask = (1l << (size * 8)) - 1;
	if ((integer & mask) == integer)
		return 1;
	if ((integer & ~mask) == (-1l & ~mask))
		return 1;
	return 0;
}

static int check_urange(IgType type, unsigned long integer)
{
	unsigned int size = igtype_get_size(type);
	unsigned long mask;
	if (size == sizeof(unsigned long))
		return 1;
	mask = (1ul << (size * 8)) - 1;
	return (integer & mask) == integer;
}

static IgType consume_type(IgParse *igparse)
{
	if (consume(igparse, TYPE_S32))
		return IGTYPE_S32;
	if (consume(igparse, TYPE_U32))
		return IGTYPE_U32;
	if (consume(igparse, TYPE_S16))
		return IGTYPE_S16;
	if (consume(igparse, TYPE_U16))
		return IGTYPE_U16;
	if (consume(igparse, TYPE_S8))
		return IGTYPE_S8;
	if (consume(igparse, TYPE_U8))
		return IGTYPE_U8;
	if (consume(igparse, TYPE_F64))
		return IGTYPE_F64;
	if (consume(igparse, TYPE_F32))
		return IGTYPE_F32;
	return IGTYPE_NONE;
}

static int parse_value(IgParse *igparse)
{
	if (consume(igparse, VALUE_MARK)) {
		IgType type = consume_type(igparse);
		if (igparse->next->type != IDENTIFIER) {
			parse_error(igparse,
				    "Expected at most one type specifier followed by value identifier, found '%s'",
				    igparse->next->repr);
			return -1;
		}
		if ((igparse->next->repr[0] >= '0' &&
		     igparse->next->repr[0] <= '9') ||
		    igparse->next->repr[0] == '-') {
			char *end = NULL;
			errno = 0;

			if (type == IGTYPE_NONE)
				type = IGTYPE_S32;

			if (is_sinttype(type)) {
				long value = strtol(
					igparse->next->repr,
					&end,
					0);
				if (!check_srange(type, (unsigned long)value)) {
					parse_error(igparse,
						    "Value out of range: %lu",
						    value);
				}
				igparse->handlers->integer(
					igparse->userdata,
					type,
					(unsigned int)value);
			} else if (is_uinttype(type)) {
				unsigned long value = strtoul(
					igparse->next->repr,
					&end,
					0);
				if (!check_urange(type, value)) {
					parse_error(igparse,
						    "Value out of range: %lu",
						    value);
				}
				igparse->handlers->integer(
					igparse->userdata,
					type,
					value);
			} else if (is_floattype(type)) {
				double value = strtod(
					igparse->next->repr,
					&end);
				igparse->handlers->real(
					igparse->userdata,
					type,
					value);
			}
			if (errno || (end && *end)) {
				parse_error(igparse,
					    "Invalid number '%s'",
					    igparse->next->repr);
			}
		} else {
			IgName *name = igname_new();
			igname_put_string(name, igparse->next->repr);
			igparse->handlers->variable(igparse->userdata,
						    name,
						    type);
			igname_unref(name);
		}
		next(igparse);
	} else if (igparse->next->type == STRING) {
		igparse->handlers->string(igparse->userdata,
					  igparse->next->repr);
		next(igparse);
	} else {
		return parse_call(igparse, 1);
	}
	if (!consume(igparse, CLOSING_BRACKET)) {
		parse_error(igparse,
			    "Expected ']', found '%s'",
			    igparse->next->repr);
		return -1;
	}
	if (consume(igparse, STORE_VALUE_TO))
		parse_store(igparse);
	return 0;
}

static int parse_jump(IgParse *igparse, IgLexTokenType type)
{
	int to_start;
	IgName *name;

	if (type != V_JUMP) {
		if (!consume(igparse, OPENING_BRACKET)) {
			parse_error(igparse,
				    "Expected '[' after jump, found '%s'",
				    igparse->next->repr);
			return -1;
		}

		if (parse_value(igparse) < 0)
			return -1;
	}

	if (consume(igparse, V_START)) {
		to_start = 1;
	} else if (consume(igparse, V_END)) {
		to_start = 0;
	} else {
		parse_error(igparse,
			    "Expected '.start' or '.end', found '%s'",
			    igparse->next->repr);
		return -1;
	}

	if (!consume(igparse, OPENING_BRACKET)) {
		parse_error(igparse,
			    "Expected '[', found '%s'",
			    igparse->next->repr);
		return -1;
	}

	name = igname_new();

	while (igparse->next->type == IDENTIFIER) {
		igname_put_string(name, igparse->next->repr);
		next(igparse);
	}

	igparse->handlers->jump(igparse->userdata, type, to_start, name);

	igname_unref(name);

	if (!consume(igparse, CLOSING_BRACKET)) {
		parse_error(igparse,
			    "Expected identifier or ']', found '%s'",
			    igparse->next->repr);
		return -1;
	}

	return 0;
}

static int parse_env(IgParse *igparse, int start)
{
	IgName *name;

	if (!consume(igparse, OPENING_BRACKET)) {
		parse_error(igparse,
			    "Expected '[', found '%s'",
			    igparse->next->repr);
		return -1;
	}

	name = igname_new();

	while (igparse->next->type == IDENTIFIER) {
		igname_put_string(name, igparse->next->repr);
		next(igparse);
	}

	if (start)
		igparse->handlers->start_env(igparse->userdata, name);
	else
		igparse->handlers->end_env(igparse->userdata, name);

	igname_unref(name);

	if (!consume(igparse, CLOSING_BRACKET)) {
		parse_error(igparse,
			    "Expected identifier or ']', found '%s'",
			    igparse->next->repr);
		return -1;
	}

	return 0;
}

static void parse_statement(IgParse *igparse)
{
	if (consume(igparse, V_JUMP_ZERO)) {
		if (parse_jump(igparse, V_JUMP_ZERO) < 0)
			goto err;
	} else if (consume(igparse, V_JUMP_NONZERO)) {
		if (parse_jump(igparse, V_JUMP_NONZERO) < 0)
			goto err;
	} else if (consume(igparse, V_JUMP)) {
		if (parse_jump(igparse, V_JUMP) < 0)
			goto err;
	} else if (consume(igparse, V_START)) {
		if (parse_env(igparse, 1) < 0)
			goto err;
	} else if (consume(igparse, V_END)) {
		if (parse_env(igparse, 0) < 0)
			goto err;
	} else if (consume(igparse, OPENING_BRACKET)) {
		if (parse_value(igparse) < 0)
			goto err;
		igparse->handlers->discard(igparse->userdata);
	} else if (consume(igparse, OPENING_DOT_BRACKET)) {
		parse_macro_call(igparse);
	} else if (consume(igparse, OPENING_UND_BRACKET)) {
		parse_call(igparse, 0);
	} else {
		parse_error(igparse, "Expected statement, found '%s'",
			    igparse->next->repr);
		goto err;
	}
	return;
err:
	while (igparse->next->type != OPENING_BRACKET &&
	       igparse->next->type != OPENING_DOT_BRACKET &&
	       igparse->next->type != OPENING_UND_BRACKET &&
	       igparse->next->type != V_JUMP_ZERO &&
	       igparse->next->type != V_JUMP_NONZERO &&
	       igparse->next->type != V_JUMP &&
	       igparse->next->type != V_START &&
	       igparse->next->type != V_END &&
	       igparse->next->type != EOF_TOKEN)
		next(igparse);
}

static void parse_macro_decl(IgParse *igparse)
{
	IgName *name = igname_new();
	IgMacro *macro = malloc(sizeof(IgMacro));

	while (igparse->next->type != EOF_TOKEN) {
		if (consume(igparse, OPENING_BRACKET)) {
			if (consume(igparse, CLOSING_BRACKET)) {
				igname_put_string(name, "[]");
			} else {
				parse_error(igparse,
					    "Expected ']', found '%s'",
					    igparse->next->repr);
				goto err;
			}
		} else if (consume(igparse, CLOSING_BRACKET)) {
			break;
		} else if (igparse->next->type == IDENTIFIER) {
			igname_put_string(name, igparse->next->repr);
			next(igparse);
		} else {
			parse_error(igparse, "Unexpected '%s' in macro name",
				    igparse->next->repr);
			goto err;
		}
	}

	if (!consume(igparse, OPENING_CURLY)) {
		parse_error(igparse, "Missing '{'");
		goto err;
	}

	// Instead of doing this:
	//  igparse->handlers->macro(igparse->userdata, name, argc);
	// We handle macros right here in the parser.

	macro->tokens = parse_token_sequence(igparse, 1);

	igtable_put(igparse->macros, name, macro);

	igname_unref(name);

	if (!consume(igparse, CLOSING_CURLY))
		parse_error(igparse, "Missing '}'");

	return;
err:
	igname_unref(name);
	while (!consume(igparse, CLOSING_CURLY) &&
	       !consume(igparse, EOF_TOKEN))
		next(igparse);
}

static void parse_decl(IgParse *igparse, int public)
{
	IgName *name = igname_new();
	unsigned int argc = 0, argbufsize = 10;
	IgArgument *args = malloc(argbufsize * sizeof(IgArgument));
	IgType return_type = IGTYPE_NONE;

	while (igparse->next->type != EOF_TOKEN) {
		if (consume(igparse, OPENING_BRACKET)) {
			igname_put_string(name, "[]");

			if (argc >= argbufsize) {
				args = realloc(args,
					       argbufsize *
					       sizeof(IgArgument));
			}

			args[argc].name = NULL;

			args[argc].type = consume_type(igparse);
			if (args[argc].type == IGTYPE_NONE) {
				parse_error(igparse,
					    "Expected type name, found '%s'",
					    igparse->next->repr);
				goto err;
			}

			if (igparse->next->type == IDENTIFIER) {
				args[argc].name = igname_new();
				igname_put_string(args[argc].name,
						  igparse->next->repr);
				next(igparse);
			}

			argc++;

			if (!consume(igparse, CLOSING_BRACKET)) {
				parse_error(igparse,
					    "Expected ], found '%s'",
					    igparse->next->repr);
				goto err;
			}
		} else if (consume(igparse, CLOSING_BRACKET)) {
			break;
		} else if (igparse->next->type == IDENTIFIER) {
			igname_put_string(name, igparse->next->repr);
			next(igparse);
		} else {
			parse_error(igparse, "Unexpected '%s' in name",
				    igparse->next->repr);
			goto err;
		}
	}

	if (consume(igparse, STORE_VALUE_TO)) {
		return_type = consume_type(igparse);
		if (return_type == IGTYPE_NONE) {
			parse_error(igparse, "Expected type name, found '%s'",
				    igparse->next->repr);
			while (igparse->next->type != OPENING_CURLY)
				next(igparse);
		}
	}

	if (consume(igparse, OPENING_CURLY)) {
		igparse->handlers->function_decl(igparse->userdata,
						 name,
						 argc,
						 args,
						 return_type,
						 public,
						 0);

		while (!consume(igparse, CLOSING_CURLY) &&
		       !consume(igparse, EOF_TOKEN))
			parse_statement(igparse);

		igparse->handlers->function_end(igparse->userdata);
	} else {
		igparse->handlers->function_decl(igparse->userdata,
						 name,
						 argc,
						 args,
						 return_type,
						 public,
						 1);
	}

	igname_unref(name);

	for (; argc; argc--) {
		igname_unref(args[argc-1].name);
	}

	free(args);

	return;
err:
	igname_unref(name);
	while (!consume(igparse, CLOSING_CURLY) &&
	       !consume(igparse, EOF_TOKEN))
		next(igparse);
}

void igparse_feed(IgParse *igparse, IgLex *iglex)
{
	igparse->lex = iglex;
	next(igparse);
	if (!igparse->next)
		return;
	while (igparse->next->type != EOF_TOKEN) {
		if (consume(igparse, OPENING_BRACKET)) {
			parse_decl(igparse, 1);
		} else if (consume(igparse, OPENING_DOT_BRACKET)) {
			parse_macro_decl(igparse);
		} else if (consume(igparse, OPENING_UND_BRACKET)) {
			parse_decl(igparse, 0);
		} else {
			parse_error(igparse,
				    "Expected '[' or '.[' or '_[', found '%s'",
				    igparse->next->repr);
			while (!consume(igparse, CLOSING_CURLY) &&
			       !consume(igparse, EOF_TOKEN))
				next(igparse);
			return;
		}
	}
}

void igparse_dispose(IgParse *igparse)
{
	iglex_token_unref(igparse->next);
	igtable_dispose(igparse->macros);
	free(igparse);
}
