/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include "igtype.h"

static const char *type_strings[] = {
	[IGTYPE_NONE]	= "none",
	[IGTYPE_S32]	= "32-bit signed",
	[IGTYPE_U32]	= "32-bit unsigned",
	[IGTYPE_S16]	= "16-bit signed",
	[IGTYPE_U16]	= "16-bit unsigned",
	[IGTYPE_S8]	= "8-bit signed",
	[IGTYPE_U8]	= "8-bit unsigned",
	[IGTYPE_F64]	= "64-bit floating-point",
	[IGTYPE_F32]	= "32-bit floating-point",
	[IGTYPE_PTR]	= "pointer",
	[IGTYPE_MAX_SIZE] = "max-size placeholder",
};

const char *igtype_to_string(IgType type)
{
	if (type < 0 || type >= (sizeof(type_strings) / sizeof(const char *))) {
		fprintf(stderr, "WARNING: invalid type %d\n", type);
		return 0;
	}
	return type_strings[type];
}

static const unsigned int type_sizes[] = {
	[IGTYPE_NONE]	= 0,
	[IGTYPE_S32]	= 4,
	[IGTYPE_U32]	= 4,
	[IGTYPE_S16]	= 2,
	[IGTYPE_U16]	= 2,
	[IGTYPE_S8]	= 1,
	[IGTYPE_U8]	= 1,
	[IGTYPE_F64]	= 8,
	[IGTYPE_F32]	= 4,
	[IGTYPE_PTR]	= 4,
	[IGTYPE_MAX_SIZE] = 8,
};

unsigned int igtype_get_size(IgType type)
{
	if (type < 0 || type >= (sizeof(type_sizes) / sizeof(unsigned int))) {
		fprintf(stderr, "WARNING: invalid type %d\n", type);
		return 0;
	}
	return type_sizes[type];
}
