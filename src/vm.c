/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <libigi/vm.h>
#include "program.h"

#define VM_INVALID_OPCODE -3
#define VM_NEW_ERROR -2
#define VM_ERROR -1
#define VM_RETURN 0
#define VM_CONTINUE 1

//#define VM_DEBUG

#ifdef VM_DEBUG
#define vm_debug(...) fprintf(stderr, ##__VA_ARGS__)
#else
#define vm_debug(...) do {} while (0)
#endif

struct _IgelVM {
	IgelProgram *prg;
	uint8_t *memory;
	size_t memory_size, n_funcs;
	const IgelVMBuiltinFunction *funcs;
	void *userdata;
	unsigned int flag : 1;
};

static void expand_memory(IgelVM *vm, size_t addr)
{
	if (addr < vm->memory_size)
		return;
	while (addr >= vm->memory_size) {
		vm->memory_size *= 2;
	}
	vm->memory = realloc(vm->memory, vm->memory_size);
}

static void handle_put(IgelVM *vm,
		       size_t fp,
		       uint32_t *i,
		       uint16_t addr)
{
	uint8_t size;
	void *frame = vm->memory + fp;
	uint16_t index;
	unpack_addr(&size, &addr, &index);
	vm_debug("PUT +0x%04x (%u*%u)\n", addr, size, index);
	vm_debug("         |  %1$08x              %1$u\n",
		 vm->prg->bytecode[*i + 1]);
	expand_memory(vm, fp + addr);
	if (size == 1) {
		((uint8_t*)frame)[index] = vm->prg->bytecode[++(*i)];
	} else if (size == 2) {
		((uint16_t*)frame)[index] = vm->prg->bytecode[++(*i)];
	} else if (size == 4) {
		((uint32_t*)frame)[index] = vm->prg->bytecode[++(*i)];
	} else if (size == 8) {
		uint32_t low = vm->prg->bytecode[++(*i)];
		((uint64_t*)frame)[index] =
			(uint64_t)vm->prg->bytecode[++(*i)] << 32 | low;
	}
}

static int handle_copy(IgelVM *vm,
		       size_t fp,
		       uint16_t dest,
		       uint16_t src)
{
	int pad = IS_BB(dest);
	uint8_t d_size, s_size;
	void *frame = vm->memory + fp;
	uint16_t d_index, s_index;
	int64_t val = 0;
	unpack_addr(&d_size, &dest, &d_index);
	unpack_addr(&s_size, &src, &s_index);
	vm_debug("COPY +0x%04x (%u*%u) <- +0x%04x (%u*%u)\n",
		 dest, d_size, d_index,
		 src, s_size, s_index);
	if (fp + src >= vm->memory_size) {
		fprintf(stderr, "Out of bounds access in COPY instruction\n");
		return -1;
	}
	expand_memory(vm, fp + dest);
	if (pad) {
		if (s_size == 1) {
			val = ((int8_t*)frame)[s_index];
		} else if (s_size == 2) {
			val = ((int16_t*)frame)[s_index];
		} else if (s_size == 4) {
			val = ((int32_t*)frame)[s_index];
		} else if (s_size == 8) {
			val = ((int64_t*)frame)[s_index];
		}
	} else {
		if (s_size == 1) {
			val = ((uint8_t*)frame)[s_index];
		} else if (s_size == 2) {
			val = ((uint16_t*)frame)[s_index];
		} else if (s_size == 4) {
			val = ((uint32_t*)frame)[s_index];
		} else if (s_size == 8) {
			val = ((uint64_t*)frame)[s_index];
		}
	}
	if (d_size == 1) {
		((uint8_t*)frame)[d_index] = val;
	} else if (d_size == 2) {
		((uint16_t*)frame)[d_index] = val;
	} else if (d_size == 4) {
		((uint32_t*)frame)[d_index] = val;
	} else if (d_size == 8) {
		((uint64_t*)frame)[d_index] = val;
	}
	return 0;
}

static int handle_test(IgelVM *vm,
		       size_t fp,
		       uint16_t addr)
{
	uint8_t size;
	void *frame = vm->memory + fp;
	uint16_t index;
	unpack_addr(&size, &addr, &index);
	vm_debug("TEST +0x%04x (%u*%u)\n", addr, size, index);
	if (fp + addr >= vm->memory_size) {
		fprintf(stderr, "Out of bounds access in TEST instruction\n");
		return -1;
	}
	expand_memory(vm, fp + addr);
	if (size == 1) {
		vm->flag = !!((uint8_t*)frame)[index];
	} else if (size == 2) {
		vm->flag = !!((uint16_t*)frame)[index];
	} else if (size == 4) {
		vm->flag = !!((uint32_t*)frame)[index];
	} else if (size == 8) {
		vm->flag = !!((uint64_t*)frame)[index];
	}
	return 0;
}

static int handle_call(IgelVM *vm,
		       size_t fp,
		       uint16_t marker);

static int interpret(IgelVM *vm,
		     size_t fp,
		     uint32_t *i)
{
	uint16_t left = LEFT(vm->prg->bytecode[*i]);
	uint16_t right = RIGHT(vm->prg->bytecode[*i]);
	vm_debug("%08x | %04x %04x | ", *i, left, right);
	if (IS_M(right)) {
		uint16_t fp_offset = GET_LOWER(right) << 3;
		vm_debug("X  M  | CALL (0x%04x) +0x%04x\n",
			 left, fp_offset);
		if (handle_call(vm, fp + fp_offset, left) < 0)
			return VM_ERROR;
	} else if (IS_N(right)) {
		uint16_t type = GET_LOWER(right);
		vm_debug("X  N  | JMP type %u\n", type);
		if (left >= vm->prg->n_markers ||
		    vm->prg->markers[left] == 0xffffffff) {
			fprintf(stderr, "Invalid marker %04x\n", left);
			return VM_NEW_ERROR;
		}
		if ((type == 0 && !vm->flag) ||
		    (type == 1 && vm->flag) ||
		    type == 2) {
			*i = vm->prg->markers[left];
			return VM_CONTINUE;
		}
	} else if (IS_AA(right) && !IS_MN(left)) {
		if (handle_copy(vm, fp, left, right) < 0)
			return VM_NEW_ERROR;
	} else if (IS_A(right) && IS_MN(left)) {
		uint16_t func = GET_HIGHER(left);
		uint16_t fp_offset = GET_LOWER(right) << 3;
		vm_debug("MN A  | EXT (%03x) +%04x\n", func, fp_offset);
		if (func >= vm->n_funcs || !vm->funcs[func])
			return VM_INVALID_OPCODE;
		vm->funcs[func](vm, fp + fp_offset, vm->userdata);
	} else if (IS_BB(right) && IS_MN(left)) {
		uint16_t subop = GET_HIGHER(left);
		vm_debug("MN BB | . ");
		switch (subop) {
		case I_VAL_PUT:	
			handle_put(vm, fp, i, right); break;
		case I_VAL_TEST:
			if (handle_test(vm, fp, right) < 0)
				return VM_NEW_ERROR;
			break;
		case I_VAL_EXT1:
			vm_debug("EXT1 ");
			switch (GET_LOWER(right)) {
			case I_VAL_EXT1_RET:
				vm_debug("RET\n");
				return VM_RETURN;
			default:
				vm_debug("???\n");
				return VM_INVALID_OPCODE;
			}
			break;
		default:
			vm_debug("???\n");
			return VM_INVALID_OPCODE;
		}
	} else {
		vm_debug("AB B? | ???\n");
		return VM_INVALID_OPCODE;
	}
	(*i)++;
	return VM_CONTINUE;
}

static int handle_call(IgelVM *vm,
		       size_t fp,
		       uint16_t marker)
{
	uint32_t i;

	if (marker > vm->prg->n_markers) {
		vm_debug("Error: Invalid marker %u!\n", marker);
		return VM_NEW_ERROR;
	}

	for (i = vm->prg->markers[marker]; i < vm->prg->last_addr;) {
		int ret = interpret(vm, fp, &i);
		if (ret == VM_INVALID_OPCODE) {
			fprintf(stderr, "Invalid opcode %08x at %08x\n",
				vm->prg->bytecode[i], i);
			return VM_ERROR;
		} else if (ret == VM_NEW_ERROR) {
			fprintf(stderr, "Error near %08x at %08x\n",
				vm->prg->bytecode[i], i);
			return VM_ERROR;
		} else if (ret != VM_CONTINUE) {
			return ret;
		}
	}
	vm_debug("Error: Program ended without returning!\n");
	return VM_ERROR;
}

#ifdef VM_DEBUG
static void memory_dump(IgelVM *vm)
{
	size_t i;
	vm_debug("** memory dump **\n");
	for (i = 0; i < vm->memory_size; i++) {
		if (i % 16 == 0) {
			vm_debug("%08zx | ", i);
		}
		vm_debug("%02x ", vm->memory[i]);
		if (i % 16 == 15) {
			vm_debug("\n");
		}
	}
}
#endif

int igvm_run_program(IgelProgram *prg,
		     uint16_t marker,
		     const IgelVMBuiltinFunction *funcs,
		     size_t n_funcs,
		     void *userdata)
{
	int ret;
	IgelVM vm;
	vm.memory_size = 256;
	vm.memory = malloc(vm.memory_size);
	vm.funcs = funcs;
	vm.n_funcs = n_funcs;
	vm.userdata = userdata;
	vm.flag = 0;
	vm.prg = prg;
	ret = handle_call(&vm, 0, marker);
#ifdef VM_DEBUG
	memory_dump(&vm);
#endif
	free(vm.memory);
	return ret;
}

double igvm_get_double(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((double*)mem);
}

void igvm_put_double(IgelVM *vm, size_t offset, double val)
{
	void *mem = vm->memory + offset;
	*((double*)mem) = val;
}

float igvm_get_float(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((float*)mem);
}

void igvm_put_float(IgelVM *vm, size_t offset, float val)
{
	void *mem = vm->memory + offset;
	*((float*)mem) = val;
}

uint64_t igvm_get64(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((uint64_t*)mem);
}

void igvm_put64(IgelVM *vm, size_t offset, uint64_t val)
{
	void *mem = vm->memory + offset;
	*((uint64_t*)mem) = val;
}

uint32_t igvm_get32(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((uint32_t*)mem);
}

void igvm_put32(IgelVM *vm, size_t offset, uint32_t val)
{
	void *mem = vm->memory + offset;
	*((uint32_t*)mem) = val;
}

uint16_t igvm_get16(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((uint16_t*)mem);
}

void igvm_put16(IgelVM *vm, size_t offset, uint16_t val)
{
	void *mem = vm->memory + offset;
	*((uint16_t*)mem) = val;
}

uint8_t igvm_get8(IgelVM *vm, size_t offset)
{
	void *mem = vm->memory + offset;
	return *((uint8_t*)mem);
}

void igvm_put8(IgelVM *vm, size_t offset, uint8_t val)
{
	void *mem = vm->memory + offset;
	*((uint8_t*)mem) = val;
}

const char *igvm_get_string(IgelVM *vm, size_t offset)
{
	uint32_t daddr = igvm_get32(vm, offset);
	if (!(daddr & VALID_ADDR) ||
	    (daddr & ~VALID_ADDR) >= vm->prg->last_daddr) {
		fprintf(stderr, "Invalid pointer: 0x%08x\n", daddr);
		return NULL;
	}
	return (char*)(vm->prg->data + (daddr & ~VALID_ADDR));
}
