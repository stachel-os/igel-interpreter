/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGPARSE_H_
#define _LIBIGI_PRIVATE_IGPARSE_H_

#include "igerror.h"
#include "iglex.h"
#include "igname.h"
#include "igtype.h"

typedef struct _IgParse IgParse;

typedef struct _IgArgument {
	IgType type;
	IgName *name;
} IgArgument;

typedef struct _IgParseHandlers {
	void (*function_decl)(void *userdata,
			      IgName *name,
			      unsigned int argc,
			      IgArgument *args,
			      IgType return_type,
			      int public,
			      int forward_decl);
	void (*function_end)(void *userdata);
	void (*function_call_prepare)(void *userdata);
	void (*function_call)(void *userdata,
			      IgName *name,
			      int public);
	void (*jump)(void *userdata,
		     IgLexTokenType type,
		     int to_start,
		     IgName *name);
	void (*string)(void *userdata,
		       const char *value);
	void (*integer)(void *userdata,
			IgType type,
			unsigned long value);
	void (*real)(void *userdata,
		     IgType type,
		     double value);
	void (*variable)(void *userdata,
			 IgName *name,
			 IgType type);
	void (*store)(void *userdata,
		      IgName *name);
	void (*discard)(void *userdata);
	void (*start_env)(void *userdata,
			  IgName *name);
	void (*end_env)(void *userdata,
			IgName *name);
} IgParseHandlers;

IgParse *igparse_create(IgelErrorStatus *err,
			const IgParseHandlers *handlers,
			void *userdata);
void igparse_feed(IgParse *igparse, IgLex *iglex);
void igparse_dispose(IgParse *igparse);

#endif
