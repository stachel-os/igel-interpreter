/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <libigi/compiler.h>
#include "igerror.h"
#include "iglex.h"
#include "igparse.h"
#include "igtable.h"
#include "program.h"

//#define COMPILER_DEBUG

typedef struct _IgRegSpec {
	IgType type;
	uint32_t series;
	uint16_t slot;
} IgRegSpec;

typedef struct _IgVariable {
	IgType type;
	uint16_t slot;
} IgVariable;

typedef struct _IgFunction {
	unsigned int builtin : 1;
	unsigned int defined : 1;
	union {
		uint16_t marker;
		uint8_t code;
	};
	IgType *argtypes;
	IgType ret_type;
	unsigned int argc;
} IgFunction;

typedef struct _IgEnvironment {
	IgName *name;
	IgTable *vars;
	uint16_t start, end;
	uint32_t sp;
	struct _IgEnvironment *parent;
} IgEnvironment;

struct _IgelCompiler {
	IgelErrorStatus err;
	IgEnvironment *env;
	IgelProgram *prg;
	IgRegSpec *stack;
	IgTable *strings;
	uint32_t stack_size, sp, series;
	uint16_t markers_size;
	uint32_t prg_size, data_size;
	int skip;
#ifdef COMPILER_DEBUG
	struct timespec ts;
#endif
};

#ifdef COMPILER_DEBUG
double measure_time(struct timespec *ts1)
{
	double difference;
	struct timespec ts2;
	clock_gettime(CLOCK_MONOTONIC, &ts2);
	if (ts1->tv_sec > ts2.tv_sec)
		return 0;
	if (ts1->tv_nsec > ts2.tv_nsec)
		return 0;
	difference = ts2.tv_sec - ts1->tv_sec;
	difference += (ts2.tv_nsec - ts1->tv_nsec) / 1e9;
	return difference;
}
#endif

static uint16_t alloc_marker(IgelCompiler *igc)
{
	if (igc->prg->n_markers == 0xffff) {
		igerror_report(&igc->err, "Error",
			       "65535 marker limit reached");
		return 0xffff;
	}
	if (igc->prg->n_markers >= igc->markers_size) {
		unsigned int old_size = igc->markers_size;
		igc->markers_size = old_size * 2;
		igc->prg->markers = realloc(igc->prg->markers,
					    sizeof(unsigned int) *
					    igc->markers_size);
		memset(igc->prg->markers + old_size, 0xff, old_size);
	}
	return igc->prg->n_markers++;
}

static void bytecode_write(IgelCompiler *igc, uint32_t code)
{
	if (igc->prg->last_addr >= igc->prg_size) {
		igc->prg_size *= 2;
		igc->prg->bytecode = realloc(igc->prg->bytecode,
					     igc->prg_size * 4);
	}
	igc->prg->bytecode[igc->prg->last_addr++] = code;
}

static uint16_t stack_push(IgelCompiler *igc, IgType type)
{
	uint32_t series = igc->stack[igc->sp].series;
	uint16_t slot = igc->stack[igc->sp].slot, aligned;
	unsigned int size = igtype_get_size(type);

	if (size != 0)
		aligned = (slot + size - 1) & ~(size - 1);
	else
		aligned = slot;

#ifdef COMPILER_DEBUG
	fprintf(stderr, "[stack] [%u] pushing %s, series=%u, slot=%u\n",
		igc->sp, igtype_to_string(type), series, aligned);
#endif

	igc->stack[igc->sp].slot = aligned;
	igc->stack[igc->sp].type = type;

	igc->sp++;

	if (igc->sp >= igc->stack_size) {
		igc->stack_size *= 2;
		igc->stack = realloc(igc->stack,
				     igc->stack_size *
				     sizeof(IgRegSpec));
	}

	igc->stack[igc->sp].series = series;
	igc->stack[igc->sp].slot = aligned + size;

	if (igc->stack[igc->sp].slot < slot) {
		igerror_report(&igc->err, "Error",
			       "%u byte stack frame size limit reached",
			       1 << 14);
	}

#ifdef COMPILER_DEBUG
	fprintf(stderr, "[stack] [%u] after push: slot=%u\n",
		igc->sp, igc->stack[igc->sp].slot);
#endif

	return aligned;
}

static uint16_t stack_pop(IgelCompiler *igc, IgType *type)
{
	if (igc->sp == 0) {
		igerror_report(&igc->err, "Internal Error",
			       "No value to pop from stack");
		if (type)
			*type = IGTYPE_NONE;
		return 0xffff;
	}
	igc->sp--;
#ifdef COMPILER_DEBUG
	fprintf(stderr, "[stack] [%u] discarded %s, series=%u, slot=%u\n",
		igc->sp,
		igtype_to_string(igc->stack[igc->sp].type),
		igc->stack[igc->sp].series,
		igc->stack[igc->sp].slot);
#endif
	if (type)
		*type = igc->stack[igc->sp].type;
	return igc->stack[igc->sp].slot;
}

static void put_value(IgelCompiler *igc, IgType type, uint64_t value)
{
	unsigned int bytes = igtype_get_size(type);
	uint16_t slot = stack_push(igc, type);
	if (bytes == 0)
		return;
	bytecode_write(igc, I_PUT(bytes, slot));
	if (bytes > 4) {
		bytecode_write(igc, value);
		value >>= 32;
	}
	bytecode_write(igc, value);
}

static void handle_function_call_prepare(void *userdata)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
	igc->stack[igc->sp].series = igc->sp;
	stack_push(igc, IGTYPE_MAX_SIZE);
#ifdef COMPILER_DEBUG
	fprintf(stderr, "FUNCTION CALL STARTED\n");
#endif
}

static void handle_function_call(void *userdata,
				 IgName *name,
				 int public)
{
	IgelCompiler *igc = userdata;
	IgFunction *func;
	uint32_t series;
	unsigned int i;
	if (igc->skip)
		return;
	series = igc->stack[igc->sp].series;
#ifdef COMPILER_DEBUG
	printf("FUNCTION_CALL (%s) {\n  ", public ? "public" : "private");
	igname_print(name);
	printf("}\n");
#endif
	if (series == 0) {
		igerror_report(&igc->err, "Internal Error",
			       "Series is 0 inside a function call");
		return;
	}
	func = igtable_get(public ? igc->prg->public : igc->prg->private, name);
	if (!func) {
		igerror_report(&igc->err, "Name Error",
			       "Undeclared function: %s",
			       igname_join(name, ' '));
		return;
	}
	if (func->builtin)
		bytecode_write(igc, I_EXT(func->code, igc->stack[series].slot));
	else
		bytecode_write(igc, I_CALL(func->marker, igc->stack[series].slot));
	for (i = 0; i < func->argc; i++) {
		IgType type = igc->stack[series+1+i].type;
		IgType arg_type = func->argtypes[i];
		if (type != arg_type &&
		    (!is_inttype(type) || !is_inttype(arg_type) ||
		     igtype_get_size(type) != igtype_get_size(arg_type))) {
			igerror_report(&igc->err, "Type Error",
				       "Expected %s as argument %i, but got %s",
				       igtype_to_string(arg_type),
				       i,
				       igtype_to_string(type));
		}
	}
	igc->stack[series].series = igc->stack[series - 1].series;
	igc->sp = series;
	stack_push(igc, func->ret_type);
#ifdef COMPILER_DEBUG
	fprintf(stderr, "[stack] [%u] after function call: series=%u, slot=%u\n",
		igc->sp, igc->stack[igc->sp].series, igc->stack[igc->sp].slot);
#endif
}

static void start_environment(IgelCompiler *igc, IgName *name)
{
	IgEnvironment *env = malloc(sizeof(IgEnvironment));
	env->name = igname_ref(name);
	env->vars = igtable_new(free);
	env->parent = igc->env;
	env->start = alloc_marker(igc);
	igc->prg->markers[env->start] = igc->prg->last_addr;
	env->sp = igc->sp;
	env->end = alloc_marker(igc);
	igc->env = env;
}

static void end_environment(IgelCompiler *igc)
{
	IgEnvironment *env = igc->env;
	igc->prg->markers[env->end] = igc->prg->last_addr;
	igc->env = env->parent;
	igname_unref(env->name);
	igtable_dispose(env->vars);
	if (igc->env)
		igc->sp = igc->env->sp;
	else
		igc->sp = 0;
#ifdef COMPILER_DEBUG
	fprintf(stderr, "[stack] [%u] environment ended, series=%u, slot=%u\n",
		igc->sp, igc->stack[igc->sp].series, igc->stack[igc->sp].slot);
#endif
	free(env);
}

static void declare_variable(IgelCompiler *igc, IgName *name, IgType type)
{
	IgVariable *var = malloc(sizeof(IgVariable));
	var->type = type;
	var->slot = stack_push(igc, type);
	igtable_put(igc->env->vars, name, var);
	igc->env->sp = igc->sp;
}

DEFINE_STATIC_NAME(function_env, "function");
DEFINE_STATIC_NAME(ret_var, "ret");

static void handle_function_decl(void *userdata,
				 IgName *name,
				 unsigned int argc,
				 IgArgument *arg,
				 IgType return_type,
				 int public,
				 int forward_decl)
{
	IgelCompiler *igc = userdata;
	IgFunction *func;
	unsigned int i;

	igc->skip = 1;

#ifdef COMPILER_DEBUG
	printf("FUNCTION_DECL (%s) {\n  ", public ? "public" : "private");
	igname_print(name);
#endif

	func = igtable_get(public ? igc->prg->public : igc->prg->private, name);
	if (!func) {
		func = malloc(sizeof(IgFunction));
		func->builtin = 0;
		func->defined = !forward_decl;
		func->marker = alloc_marker(igc);
		func->argc = argc;
		func->argtypes = calloc(argc, sizeof(IgType));
		func->ret_type = return_type;
		igtable_put(public ? igc->prg->public : igc->prg->private,
			    name, func);
	} else {
		if (func->builtin) {
			igerror_report(&igc->err, "Type Error",
				       "Redeclaration of builtin function");
			return;
		}
		if (func->defined) {
			igerror_report(&igc->err, "Type Error",
				       "Redeclaration of already defined function");
			return;
		}
		if (func->ret_type != return_type) {
			igerror_report(&igc->err, "Type Error",
				       "Redeclaration with different return type");
		}
		/* argc must match number of bracket pairs in name */
		assert(func->argc == argc);
	}

	if (!forward_decl) {
		IgVariable *retvar;

		igc->prg->markers[func->marker] = igc->prg->last_addr;
		start_environment(igc, get_function_env_name());

		retvar = malloc(sizeof(IgVariable));
		retvar->type = func->ret_type;
		retvar->slot = stack_push(igc, IGTYPE_MAX_SIZE);
		igtable_put(igc->env->vars, get_ret_var_name(), retvar);
		igc->env->sp = igc->sp;
	}

	for (i = 0; i < argc; i++, arg++) {
#ifdef COMPILER_DEBUG
		printf("  * argument %u type %s\n    ", argc,
		       igtype_to_string(arg->type));
		if (arg->name)
			igname_print(arg->name);
		else
			printf("<anonymous>\n");
#endif
		if (!func->defined && func->argtypes[i] != arg->type) {
			igerror_report(&igc->err, "Type Error",
				       "Redeclaration with different arguments");
		}
		func->argtypes[i] = arg->type;
		if (!forward_decl) {
			if (!arg->name) {
				igerror_report(&igc->err, "Syntax Error",
					       "Function definition needs argument names");
			} else {
				declare_variable(igc, arg->name, arg->type);
			}
		}
	}

#ifdef COMPILER_DEBUG
	printf("}\n");
#endif

	igc->skip = 0;
}

static void handle_function_end(void *userdata)
{
	IgelCompiler *igc = userdata;
	if (igc->skip) {
		igc->skip = 0;
		return;
	}
	/* handle_end_env ensures that nobody ends the function environment
	 * before we do here */
	assert(igc->env);
	if (!igname_equal(igc->env->name, get_function_env_name())) {
		igerror_report(&igc->err, "Scope Error",
			       "Function ended but an environment is still open");
		return;
	}
	end_environment(igc);
	bytecode_write(igc, I_RET);

	if (igc->sp != 0) {
		igerror_report(&igc->err, "Internal Error",
			       "Function ended but stack not empty");
	}
}

static void handle_jump(void *userdata,
			IgLexTokenType type,
			int to_start,
			IgName *name)
{
	IgelCompiler *igc = userdata;
	IgEnvironment *env = igc->env;
	uint16_t marker;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("JUMP: %s -> %s: {\n  ",
	       type == V_JUMP_ZERO ? "if zero" :
	       type == V_JUMP_NONZERO ? "if nonzero" :
	       type == V_JUMP ? "always" :
	       "unknown",
	       to_start ? "(start)" : "(end)");
	igname_print(name);
	printf("}\n");
#endif

	while (env && !igname_equal(env->name, name))
		env = env->parent;

	if (!env) {
		igerror_report(&igc->err, "Scope Error",
			       "Invalid environment specified in jump");
		return;
	}
	if (to_start)
		marker = env->start;
	else
		marker = env->end;
#ifdef COMPILER_DEBUG
	printf("Jump will take us to marker %u\n", marker);
#endif

	if (type == V_JUMP_ZERO || type == V_JUMP_NONZERO) {
		IgType vtype;
		uint16_t slot = stack_pop(igc, &vtype);
		unsigned int size = igtype_get_size(vtype);
		if (size == 0)
			type = V_JUMP;
		else
			bytecode_write(igc, I_TEST(size, slot));
	}

	bytecode_write(igc, I_JMP(marker,
				  type == V_JUMP_ZERO ? 0 :
				  type == V_JUMP_NONZERO ? 1 : 2));
}

static void handle_string(void *userdata, const char *str)
{
	IgelCompiler *igc = userdata;
	IgName *name;
	uint32_t addr;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("STRING {%s}\n", str);
#endif
	name = igname_new();
	igname_put_string(name, str);
	addr = (uintptr_t)igtable_get(igc->strings, name);
	if (addr) {
		put_value(igc, IGTYPE_PTR, addr);
	} else {
		addr = igc->prg->last_daddr;
		igc->prg->last_daddr += strlen(str) + 1;
		if (igc->prg->last_daddr >= igc->data_size) {
			while (igc->prg->last_daddr >= igc->data_size)
				igc->data_size *= 2;
			igc->prg->data = realloc(igc->prg->data,
						 igc->data_size);
		}
		memcpy(igc->prg->data + addr, str, igc->prg->last_daddr - addr);
		addr |= VALID_ADDR;
		igtable_put(igc->strings, name, (void*)(uintptr_t)addr);
		put_value(igc, IGTYPE_PTR, addr);
	}
	igname_unref(name);
}

static void handle_integer(void *userdata,
			   IgType type,
			   unsigned long value)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("%s: %ld\n", igtype_to_string(type), value);
#endif
	put_value(igc, type, value);
}

static void handle_real(void *userdata,
			IgType type,
			double value)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("%s: %f\n", igtype_to_string(type), value);
#endif
	if (type == IGTYPE_F32) {
		float f = value;
		uint32_t dest;
		memcpy(&dest, &f, 4);
		put_value(igc, type, dest);
	} else if (type == IGTYPE_F64) {
		uint64_t dest;
		memcpy(&dest, &value, 8);
		put_value(igc, type, dest);
	}
}

static void convert_copy(IgelCompiler *igc,
			 uint16_t to, uint16_t from,
			 IgType new_type, IgType old_type)
{
	unsigned int old_size, new_size;
	old_size = igtype_get_size(old_type);
	if (old_type != new_type) {
		if (!is_inttype(old_type) || !is_inttype(new_type)) {
			igerror_report(&igc->err, "Type Error",
				       "Cannot convert %s to type %s",
				       igtype_to_string(old_type),
				       igtype_to_string(new_type));
			return;
		}
		new_size = igtype_get_size(new_type);
	} else {
		new_size = old_size;
	}
	if (new_size == 0)
		return;
	if (new_size > old_size && is_sinttype(new_type))
		bytecode_write(igc, I_PAD(new_size, to, old_size, from));
	else
		bytecode_write(igc, I_COPY(new_size, to, old_size, from));
}

static void handle_store(void *userdata,
			 IgName *name)
{
	IgelCompiler *igc = userdata;
	IgEnvironment *env = igc->env;
	IgVariable *var = NULL;
	IgRegSpec *val = igc->stack + igc->sp - 1;
	if (igc->skip)
		return;
	if (igc->sp == 0) {
		fprintf(stderr,
			"Internal error: no value pushed, cannot store\n");
		return;
	}
	igtype_get_size(val->type);
#ifdef COMPILER_DEBUG
	printf("STORE into {\n  ");
	igname_print(name);
	printf("}\n");
#endif
	while (env) {
		var = igtable_get(env->vars, name);
		if (var)
			break;
		env = env->parent;
	}
	if (!var) {
		igerror_report(&igc->err, "Name Error",
			       "Undeclared variable '%s'",
			       igname_join(name, ' '));
		return;
	}
	convert_copy(igc, var->slot, val->slot, var->type, val->type);
}

static void handle_discard(void *userdata)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
	stack_pop(igc, NULL);
}

static void handle_variable(void *userdata,
			    IgName *name,
			    IgType type)
{
	IgelCompiler *igc = userdata;
	IgEnvironment *env = igc->env;
	IgVariable *var = NULL;
	uint16_t slot;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	if (type == IGTYPE_NONE)
		printf("VARIABLE {\n  ");
	else
		printf("VARIABLE as %s {\n  ", igtype_to_string(type));
	igname_print(name);
	printf("}\n");
#endif
	while (env) {
		var = igtable_get(env->vars, name);
		if (var)
			break;
		env = env->parent;
	}
	if (!var) {
		igerror_report(&igc->err, "Name Error",
			       "Undeclared variable '%s'",
			       igname_join(name, ' '));
		return;
	}
	if (type == IGTYPE_NONE)
		type = var->type;
	slot = stack_push(igc, type);
	convert_copy(igc, slot, var->slot, type, var->type);
}

static void handle_start_env(void *userdata,
			     IgName *name)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("START_ENV {\n  ");
	igname_print(name);
	printf("}\n");
#endif
	if (igname_equal(name, get_function_env_name())) {
		igerror_report(&igc->err, "Scope Error",
			       "Function environment may not be opened manually");
		return;
	}
	start_environment(igc, name);
}

static void handle_end_env(void *userdata,
			   IgName *name)
{
	IgelCompiler *igc = userdata;
	if (igc->skip)
		return;
#ifdef COMPILER_DEBUG
	printf("END_ENV {\n  ");
	igname_print(name);
	printf("}\n");
#endif
	/* FIXME these should report the line and file name */
	if (!igc->env) {
		igerror_report(&igc->err, "Scope Error",
			       "No environment to close. Perhaps there is a missing .start somewhere?");
		return;
	}
	if (igname_equal(name, get_function_env_name())) {
		igerror_report(&igc->err, "Scope Error",
			       "Function environment may not be closed manually");
		return;
	}
	if (!igname_equal(igc->env->name, name)) {
		igerror_report(&igc->err, "Scope Error",
			       "Closing environment name mismatch. Perhaps there is a missing .start or .end somewhere?");
		return;
	}
	end_environment(igc);
}

const IgParseHandlers parse_handlers = {
	.function_decl = handle_function_decl,
	.function_end = handle_function_end,
	.function_call_prepare = handle_function_call_prepare,
	.function_call = handle_function_call,
	.jump = handle_jump,
	.string = handle_string,
	.integer = handle_integer,
	.real = handle_real,
	.store = handle_store,
	.discard = handle_discard,
	.variable = handle_variable,
	.start_env = handle_start_env,
	.end_env = handle_end_env,
};

static void destroy_func(void *data)
{
	IgFunction *func = data;
	free(func->argtypes);
	free(func);
}

void igc_free_program(IgelProgram *prg)
{
	if (!prg)
		return;
	igtable_dispose(prg->public);
	igtable_dispose(prg->private);
	free(prg->data);
	free(prg->bytecode);
	free(prg->markers);
	free(prg);
}

uint16_t igc_resolve_function(IgelProgram *prg,
			      const char *name_)
{
	IgName *name = igname_new();
	IgFunction *func;
	igname_put_string(name, name_);
	igname_split(name, ' ');
	func = igtable_get(prg->public, name);
	igname_unref(name);
	return (func && !func->builtin) ? func->marker : 0xffff;
}

IgelProgram *igc_compile(FILE *stream,
			 const char *name,
			 IgelIncludeFunc include_file,
			 void *userdata,
			 const IgelBuiltinFunction **funcs,
			 size_t n_funcs)
{
	IgelCompiler igc = { 0 };
	IgParse *igparse = igparse_create(&igc.err, &parse_handlers, &igc);
	IgLex *iglex = iglex_create(&igc.err, stream, 0,
				    name, include_file, userdata);
	IgelProgram *program = malloc(sizeof(IgelProgram));
	uint32_t i;

#ifdef COMPILER_DEBUG
	clock_gettime(CLOCK_MONOTONIC, &igc.ts);
#endif

	igc.markers_size = 64;
	igc.prg_size = 128;
	igc.data_size = 128;
	igc.stack_size = 32;
	igc.stack = malloc(sizeof(IgRegSpec) * igc.stack_size);
	igc.stack[0].slot = 0;
	igc.strings = igtable_new(NULL);

	program->public = igtable_new(destroy_func);
	program->private = igtable_new(destroy_func);
	program->n_markers = 0;
	program->markers = malloc(sizeof(unsigned int) * igc.markers_size);
	memset(program->markers, 0xff, igc.markers_size);
	program->last_addr = 0;
	program->bytecode = malloc(igc.prg_size * 4);
	program->last_daddr = 0;
	program->data = malloc(igc.data_size);

	while (n_funcs) {
		IgFunction *func;
		IgName *name;
		char arg;
		size_t j, i = n_funcs-1;
		if (!funcs[i])
			continue;
		func = malloc(sizeof(IgFunction));
		func->builtin = 1;
		func->defined = 1;
		func->code = i;
		func->argc = strlen(funcs[i]->argline);
		func->argtypes = calloc(func->argc, sizeof(IgType));
		for (j = 0; (arg = funcs[i]->argline[j]); j++) {
			func->argtypes[j] = igtype_from_char(arg);
		}
		func->ret_type = igtype_from_char(funcs[i]->return_type);
		name = igname_new();
		igname_put_string(name, funcs[i]->name);
		igname_split(name, ' ');
		igtable_put(program->public, name, func);
		igname_unref(name);
		n_funcs--;
	}

	igc.prg = program;

	igparse_feed(igparse, iglex);

	while (igc.env)
		end_environment(&igc);

	igc.prg = NULL;

	iglex_dispose(iglex);
	igparse_dispose(igparse);

	if (igc.err.num_errors == 0) {
		for (i = 0; i < program->n_markers; i++) {
#ifdef COMPILER_DEBUG
			fprintf(stderr, "Marker 0x%04x -> 0x%08x\n",
				i, program->markers[i]);
#endif
			if (program->markers[i] == 0xffffffff) {
				igerror_report(&igc.err, "Error",
					       "Marker 0x%04x never defined", i);
			}
		}
	}

	free(igc.stack);
	igtable_dispose(igc.strings);

	if (igc.err.num_errors > 0) {
		fprintf(stderr, "Total error count: %d\n",
			igc.err.num_errors);
		igc_free_program(program);
		return NULL;
	}

	if (igc.sp != 0) {
		igerror_report(&igc.err, "Internal Error",
			       "Program ended but stack not empty");
	}

#ifdef COMPILER_DEBUG
	fprintf(stderr, "Compiled program in %.6f seconds.\n",
		measure_time(&igc.ts));
#endif

	return program;
}
