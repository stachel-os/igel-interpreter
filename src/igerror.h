/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGERROR_H_
#define _LIBIGI_PRIVATE_IGERROR_H_

typedef struct _IgelErrorStatus {
	int num_errors;
} IgelErrorStatus;

#define igerror_report(_e, _type, _fmt, ...) \
	((_e)->num_errors += 1, \
	fprintf(stderr, "%s: " _fmt "\n", \
		(_type), ##__VA_ARGS__))

#endif
