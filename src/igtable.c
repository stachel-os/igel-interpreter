/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#ifdef IGTABLE_DEBUG
#include <stdio.h>
#endif
#include <assert.h>
#include "igtable.h"

#define INITIAL_SIZE 31
#define GROWTH_RATE 4
#define GROWTH_THRESHOLD(size, hash) (((hash) + ((size) * 3 / 4)) % (size))

typedef struct _IgTableEntry {
	IgName *key;
	void *value;
} IgTableEntry;

struct _IgTable {
	IgTableValueDestroyFunc *vdf;
	size_t size;
	IgTableEntry *entries;
};

IgTable *igtable_new(IgTableValueDestroyFunc *vdf)
{
	IgTable *table = malloc(sizeof(IgTable));
	table->vdf = vdf;
	table->size = INITIAL_SIZE;
	table->entries = calloc(table->size, sizeof(IgTableEntry));
	return table;
}

static void grow_table(IgTable *table)
{
	size_t i, oldsize = table->size;
	IgTableEntry *oldentries = table->entries;

	table->size = oldsize * GROWTH_RATE - 1;
	table->entries = calloc(table->size, sizeof(IgTableEntry));

#ifdef IGTABLE_DEBUG
	fprintf(stderr, "[igtable] growing table: %zu -> %zu\n",
		oldsize, table->size);
#endif

	for (i = 0; i < oldsize; i++) {
		if (oldentries[i].value == NULL) {
			igname_unref(oldentries[i].key);
			continue;
		}
		assert(oldentries[i].key);
		igtable_put(table, oldentries[i].key, oldentries[i].value);
		igname_unref(oldentries[i].key);
	}
}

void igtable_put(IgTable *table, IgName *key, void *value)
{
	size_t hash = key->hash % table->size, i = hash;

	while (table->entries[i].key &&
	       table->entries[i].value &&
	       !igname_equal(table->entries[i].key, key)) {
#ifdef IGTABLE_DEBUG
		fprintf(stderr, "[igtable] slot %zu full\n", i);
#endif
		if (i == GROWTH_THRESHOLD(table->size, hash)) {
			grow_table(table);
			i = hash = key->hash % table->size;
		} else {
			i = (i + 1) % table->size;
		}
	}

#ifdef IGTABLE_DEBUG
	fprintf(stderr, "[igtable] put: key=%p, hash=%08x [%zu]=%p\n",
		key, key->hash, i, value);
#endif

	if (table->vdf && table->entries[i].value)
		table->vdf(table->entries[i].value);

	if (!table->entries[i].key)
		table->entries[i].key = igname_ref(key);

	table->entries[i].value = value;
}

void *igtable_get(IgTable *table, IgName *key)
{
	size_t hash = key->hash % table->size, i = hash;

	while (table->entries[i].key &&
	       !igname_equal(table->entries[i].key, key)) {
		i = (i + 1) % table->size;
		if (i == hash)
			return NULL;
	}

#ifdef IGTABLE_DEBUG
	fprintf(stderr, "[igtable] get: hash=%08x slot=%zu [%p]=%p\n",
		key->hash, i, &(table->entries[i].value),
		table->entries[i].value);
#endif

	if (table->entries[i].key)
		return table->entries[i].value;

	return NULL;
}

void igtable_dispose(IgTable *table)
{
	size_t i;
	for (i = 0; i < table->size; i++) {
		if (table->entries[i].key) {
			igname_unref(table->entries[i].key);
			if (table->vdf)
				table->vdf(table->entries[i].value);
		}
	}
	free(table->entries);
	free(table);
}
