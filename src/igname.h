/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGNAME_H_
#define _LIBIGI_PRIVATE_IGNAME_H_

#include <stdint.h>

#define IGNAME_STRUCT \
	uint16_t size, tail; \
	char *data; \
	uint32_t hash

#ifdef IGNAME_PRIVATE
typedef struct _IgName IgName;
#else
typedef const struct _IgName {
	IGNAME_STRUCT;
} IgName;
#undef IGNAME_STRUCT
#endif

IgName *igname_new(void);
int igname_equal(IgName *a, IgName *b);
void igname_put_string(IgName *name, const char *str);
const char *igname_join(IgName *name, char ch);
void igname_split(IgName *name, char ch);
void igname_print(IgName *name);
IgName *igname_ref(IgName *name);
void igname_unref(IgName *name);

#define DEFINE_STATIC_NAME(_what, _str) \
	static IgName *get_##_what##_name(void) \
	{ \
		static IgName *name = NULL; \
		if (!name) { \
			name = igname_new(); \
			igname_put_string(name, _str); \
		} \
		return name; \
	}

#endif
