/*
 * Copyright (C) 2023 Affe Null

 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LIBIGI_PRIVATE_IGLEX_H_
#define _LIBIGI_PRIVATE_IGLEX_H_

#include <stdio.h>
#include <libigi/compiler.h>
#include "igerror.h"

typedef enum _IgLexTokenType {
	OPENING_BRACKET,
	OPENING_DOT_BRACKET,
	OPENING_UND_BRACKET,
	CLOSING_BRACKET,
	OPENING_CURLY,
	CLOSING_CURLY,
	VALUE_MARK,
	ARG_REF,
	IDENTIFIER,
	STORE_VALUE_TO,
	V_JUMP_ZERO,
	V_JUMP_NONZERO,
	V_JUMP,
	V_START,
	V_END,
	TYPE_S32,
	TYPE_U32,
	TYPE_S16,
	TYPE_U16,
	TYPE_S8,
	TYPE_U8,
	TYPE_F64,
	TYPE_F32,
	STRING,
	INCLUDE,
	EOF_TOKEN,
} IgLexTokenType;

typedef struct _IgLex IgLex;
typedef struct _IgLexTokenPrivate IgLexTokenPrivate;

#define IGLEX_TOKEN_STRUCT \
	const char *file; \
	int line; \
	IgLexTokenType type; \
	char *repr \

#ifdef IGLEX_PRIVATE
typedef struct _IgLexToken IgLexToken;
#else
typedef const struct _IgLexToken {
	IGLEX_TOKEN_STRUCT;
} IgLexToken;
#undef IGLEX_TOKEN_STRUCT
#endif

IgLex *iglex_create(IgelErrorStatus *err,
		    FILE *stream,
		    int close_stream,
		    const char *name,
		    IgelIncludeFunc include_file,
		    void *userdata);
IgLexToken *iglex_token_ref(IgLexToken *token);
void iglex_token_unref(IgLexToken *token);
IgLexToken *iglex_next_token(IgLex *iglex);
void iglex_dispose(IgLex *iglex);

#define igerror_report_token(_e, _type, _token, _fmt, ...) \
	igerror_report(_e, _type, "(in %s, line %d): " _fmt, \
		       (_token)->file, (_token)->line, ##__VA_ARGS__)

#endif
