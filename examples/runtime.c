/*
 * Copyright (C) 2023 Affe Null

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sysexits.h>
#include <assert.h>
#include <libigi/compiler.h>
#include <libigi/vm.h>

static const IgelBuiltinFunction *funcs[] = {
	&(IgelBuiltinFunction){ "ZZ", "[] + []", 'Z' },
	&(IgelBuiltinFunction){ "ZZ", "[] - []", 'Z' },
	&(IgelBuiltinFunction){ "zz", "[] z+z []", 'z' },
	&(IgelBuiltinFunction){ "zz", "[] z-z []", 'z' },
	&(IgelBuiltinFunction){ "RR", "[] R+R []", 'R' },
	&(IgelBuiltinFunction){ "RR", "[] R-R []", 'R' },
	&(IgelBuiltinFunction){ "Z", "printZ []", ' ' },
	&(IgelBuiltinFunction){ "^", "printText []", ' ' },
	&(IgelBuiltinFunction){ "", "printNl", ' ' },
	&(IgelBuiltinFunction){ "", "test ok", ' ' },
	&(IgelBuiltinFunction){ "", "test failed", ' ' }
};

#define N_FUNCS (sizeof(funcs) / sizeof(IgelBuiltinFunction *))

static void do_add(IgelVM *vm, size_t fp, void *data)
{
	int32_t op1 = igvm_get32(vm, fp + 8);
	int32_t op2 = igvm_get32(vm, fp + 12);
	printf("Performing calculation: %d + %d\n", op1, op2);
	igvm_put32(vm, fp, op1 + op2);
}

static void do_sub(IgelVM *vm, size_t fp, void *data)
{
	int32_t op1 = igvm_get32(vm, fp + 8);
	int32_t op2 = igvm_get32(vm, fp + 12);
	printf("Performing calculation: %d - %d\n", op1, op2);
	igvm_put32(vm, fp, op1 - op2);
}

static void do_add_short(IgelVM *vm, size_t fp, void *data)
{
	int16_t op1 = igvm_get16(vm, fp + 8);
	int16_t op2 = igvm_get16(vm, fp + 10);
	printf("Performing short calculation: %d + %d\n", op1, op2);
	igvm_put16(vm, fp, op1 + op2);
}

static void do_sub_short(IgelVM *vm, size_t fp, void *data)
{
	int16_t op1 = igvm_get16(vm, fp + 8);
	int16_t op2 = igvm_get16(vm, fp + 10);
	printf("Performing short calculation: %d - %d\n", op1, op2);
	igvm_put16(vm, fp, op1 - op2);
}

static void do_add_double(IgelVM *vm, size_t fp, void *data)
{
	double op1 = igvm_get_double(vm, fp + 8);
	double op2 = igvm_get_double(vm, fp + 16);
	printf("Performing floating-point calculation: %f + %f\n", op1, op2);
	igvm_put_double(vm, fp, op1 + op2);
}

static void do_sub_double(IgelVM *vm, size_t fp, void *data)
{
	double op1 = igvm_get_double(vm, fp + 8);
	double op2 = igvm_get_double(vm, fp + 16);
	printf("Performing floating-point calculation: %f - %f\n", op1, op2);
	igvm_put_double(vm, fp, op1 - op2);
}

static void do_printZ(IgelVM *vm, size_t fp, void *data)
{
	printf("%d", (int32_t)igvm_get32(vm, fp + 8));
}

static void do_printText(IgelVM *vm, size_t fp, void *data)
{
	printf("%s", igvm_get_string(vm, fp + 8));
}

static void do_printNl(IgelVM *vm, size_t fp, void *data)
{
	printf("\n");
}

static void report_test_ok(IgelVM *vm, size_t fp, void *data)
{
	int *ok = data;
	if (*ok == 0)
		return;
	printf("Yes! It's working!\n");
	*ok = 1;
}

static void report_test_failed(IgelVM *vm, size_t fp, void *data)
{
	int *ok = data;
	printf("Oh no! Something has gone wrong!\n");
	*ok = 0;
}

static const IgelVMBuiltinFunction vm_funcs[N_FUNCS] = {
	do_add,
	do_sub,
	do_add_short,
	do_sub_short,
	do_add_double,
	do_sub_double,
	do_printZ,
	do_printText,
	do_printNl,
	report_test_ok,
	report_test_failed,
};

int main(int argc, const char **argv)
{
	int ret = EX_DATAERR, ok = -1;
	IgelProgram *prog;
	const char *filename;
	FILE *file;
	if (argc > 1) {
		filename = argv[1];
		file = fopen(filename, "r");
		if (!file) {
			perror(filename);
			return EX_NOINPUT;
		}
	} else {
		filename = "<stdin>";
		file = stdin;
	}
	prog = igc_compile(file, filename, NULL, NULL, funcs, N_FUNCS);
	if (prog) {
		uint16_t ep = igc_resolve_function(prog, "main");
		if (ep == 0xffff) {
			printf("Entry point [main] not found\n");
		} else {
			if (igvm_run_program(prog,
					     ep,
					     vm_funcs,
					     N_FUNCS,
					     &ok) < 0)
				printf("Fatal error encountered\n");
			else if (ok == -1)
				printf("[test ok] never called\n");
			else if (ok)
				ret = EX_OK;
		}
		igc_free_program(prog);
	}
	return ret;
}
